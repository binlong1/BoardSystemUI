package com.android.systemui.statusbar.board;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.util.Log;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.Handler;

import com.android.systemui.R;

import java.util.List;

public class ConnectPhoneDialog extends BaseDialog {

    private ImageView ivQrCode;
    private LinearLayout llNetTip;
    private TextView tvNetSetting;
    private InternetDialog internetDialog;
    private Intent intent;
    private TextView loading;
    private String loadingPoint;
    private int loadingCount = 0;

    private BroadcastReceiver mBoardReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (TextUtils.equals("android.intent.board.CONNECT_TOKEN", action)) {
                if (TextUtils.equals("success", intent.getStringExtra("result"))) {
                    String token = intent.getStringExtra("token");
                    Log.e("ConnectPhoneDialog", "connect token :" + token);
                    if (TextUtils.isEmpty(token)) {
                        llNetTip.setVisibility(View.VISIBLE);
                        ivQrCode.setBackgroundResource(R.drawable.qrcode_no_net);
                        return;
                    }
                    Bitmap bitmap = QRCodeEncoderUtil.syncEncodeQRCode(token, BoardUtil.dip2px(100));
                    llNetTip.setVisibility(View.GONE);
                    ivQrCode.setImageBitmap(bitmap);
                } else {
                    llNetTip.setVisibility(View.VISIBLE);
                    ivQrCode.setBackgroundResource(R.drawable.qrcode_no_net);
                }
            }
        }
    };

    public ConnectPhoneDialog(Context context) {
        super(context);
        registerBoardReceiver();
    }

    private void registerBoardReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.board.CONNECT_TOKEN");
        mContext.registerReceiver(mBoardReceiver, filter, null, null);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.pop_setting_connect_phone;
    }

    @Override
    protected void initView(View view) {
        TextView tvTitle = (TextView) view.findViewById(R.id.tv_title);
        tvTitle.setText(Html.fromHtml("<b>" + mContext.getResources().getString(R.string.str_connect_phone)
                + "</b>" + mContext.getResources().getString(R.string.str_remote_mirror_signin)));
        ivQrCode = (ImageView) view.findViewById(R.id.iv_qrcode);
        llNetTip = (LinearLayout) view.findViewById(R.id.ll_net_tip);
        tvNetSetting = (TextView) view.findViewById(R.id.tv_net_setting);
        loading = (TextView) view.findViewById(R.id.loading);
        loadingCount = 0;
        handler.postDelayed(loadingRunnable, 0);
        setClickListener(tvNetSetting);
    }

    @Override
    public void show() {
        super.show();
        llNetTip.setVisibility(View.GONE);
        getLinkQrCodeUrl();
    }

    @Override
    public void dismiss() {
        if (null != mDialog) mDialog.dismiss();
        handler.removeCallbacks(loadingRunnable);
    }

    private Handler handler = new Handler();
    private Runnable loadingRunnable = new Runnable() {
        @Override
        public void run() {
            if (loadingCount > 3) {
                loadingCount = 0;
            }
            if (loadingCount == 0) {
                loadingPoint = "   ";
            } else if (loadingCount == 1) {
                loadingPoint = ".  ";
            } else if (loadingCount == 2) {
                loadingPoint = ".. ";
            } else if (loadingCount == 3) {
                loadingPoint = "...";
            }
            loadingCount = loadingCount + 1;
            loading.setText(mContext.getResources().getString(R.string.getting_qr_code) + loadingPoint);
            handler.postDelayed(loadingRunnable, 500);
        }
    };

    private void getLinkQrCodeUrl() {
        if (intent == null) intent = new Intent("com.w2here.board.QRCODE_CONNECT");
        mContext.sendBroadcast(intent);
    }

    @Override
    public void onViewClick(View v) {
        if (v == tvNetSetting)
            getInternetDialog().show();
    }

    private InternetDialog getInternetDialog() {
        if (internetDialog == null)
            internetDialog = new InternetDialog(mContext);
        return internetDialog;
    }

}
