package com.android.systemui.statusbar.board;

import android.app.Service;
import android.content.Context;
import android.media.AudioManager;
import android.widget.ImageView;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by drolmen on 2017/8/23.
 */
public class VolumeImageView extends ImageView {

    /**
     * 上一个被选中的音量等级
     */
    private int mBeforeLevel = -1;

    /**
     * 当前音量等级，从0~4
     */
    private int mCurrentLevel = -1;

    /**
     * 每根柱子的宽度的值
     * 每个空隙的宽度值的2倍
     */
    private int per_width = 0;

    private int mMaxVolume;

    private AudioManager mAudioManager;

    public VolumeImageView(Context context) {
        super(context);
        init();
    }

    public VolumeImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        //获取当前系统音量，初始化level
        mAudioManager = (AudioManager) getContext().getSystemService(Service.AUDIO_SERVICE);
        mMaxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        if (currentVolume == 0) {
            mCurrentLevel = 0;
        } else if (currentVolume > 0 && currentVolume < mMaxVolume / 4) {
            mCurrentLevel = 1;
        } else if (currentVolume >= mMaxVolume / 4 && currentVolume < mMaxVolume / 2) {
            mCurrentLevel = 2;
        } else if (currentVolume >= mMaxVolume / 2 && currentVolume < mMaxVolume * 3 / 4) {
            mCurrentLevel = 3;
        } else if (currentVolume >= mMaxVolume * 3 / 4 && currentVolume <= mMaxVolume) {
            mCurrentLevel = 4;
        }
        mBeforeLevel = mCurrentLevel;
        setImageLevel(mCurrentLevel);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        per_width = w / 7;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean result = false;
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            result = true;
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            //音量判断
            int newLevel = retrieveLevel(event.getX(), event.getY());
            if (newLevel != mCurrentLevel) {
                mBeforeLevel = mCurrentLevel;
                mCurrentLevel = newLevel;
                setImageLevel(mCurrentLevel);
                adjustVolume();
            } else if (mCurrentLevel == 0) {
                mCurrentLevel = mBeforeLevel;
                mBeforeLevel = 0;
                setImageLevel(mCurrentLevel);
                adjustVolume();
            }
        }

        return result;
    }

    private void adjustVolume() {
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                (int) (mMaxVolume * 1.0 * mCurrentLevel / 4), AudioManager.FX_FOCUS_NAVIGATION_UP);
    }

    /**
     * 根据坐标计算被选中的音量等级
     */
    private int retrieveLevel(float x, float y) {
        int result = 0;
        if (x <= per_width + per_width / 4) {    //等级0
            result = 0;
        } else if (x > per_width + per_width / 4 && x <= per_width * 2 + per_width / 2 + per_width / 4) {   //等级1
            result = 1;
        } else if (x > per_width * 2 + per_width / 2 + per_width / 4 && x <= per_width * 4 + per_width / 4) {   //等级2
            result = 2;
        } else if (x > per_width * 4 + per_width / 4 && x < per_width * 5 + per_width / 2 + per_width / 4) {    //等级3
            result = 3;
        } else {    //等级4
            result = 4;
        }
        return result;
    }

    /**
     * 音量调节的回调
     */
    public interface VolumeCallBack {
        void onVolumeChange(int newVolume);
    }
}
