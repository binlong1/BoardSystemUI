package com.android.systemui.language;

/**
 * Created by drolmen on 2018/2/5.
 */

import android.content.Context;
import android.content.res.Configuration;

import java.lang.reflect.Method;
import java.util.Locale;

/**
 *
 */
@SuppressWarnings("unchecked")
public class LanguageUtils {

    public static void switchLanguage(Locale locale) {
        updateLanguage(locale);
    }

    private static void updateLanguage(Locale locale) {
        try {
            Object objIActMag, objActMagNative;

            Class clzIActMag = Class.forName("android.app.IActivityManager");

            Class clzActMagNative = Class
                    .forName("android.app.ActivityManagerNative");

            Method mtdActMagNative$getDefault = clzActMagNative
                    .getDeclaredMethod("getDefault");

            objIActMag = mtdActMagNative$getDefault.invoke(clzActMagNative);

            Method mtdIActMag$getConfiguration = clzIActMag
                    .getDeclaredMethod("getConfiguration");

            Configuration config = (Configuration) mtdIActMag$getConfiguration
                    .invoke(objIActMag);

            config.locale = locale;

            Class clzConfig = Class
                    .forName("android.content.res.Configuration");
            java.lang.reflect.Field userSetLocale = clzConfig
                    .getField("userSetLocale");
            userSetLocale.set(config, true);

            // 此处需要声明权限:android.permission.CHANGE_CONFIGURATION
            // 会重新调用 onCreate();
            Class[] clzParams = {Configuration.class};

            Method mtdIActMag$updateConfiguration = clzIActMag
                    .getDeclaredMethod("updateConfiguration", clzParams);

            mtdIActMag$updateConfiguration.invoke(objIActMag, config);

//			BackupManager.dataChanged("com.android.providers.settings");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static Locale getCurrentLaungage(Context context) {
        return context.getResources().getConfiguration().locale;
    }

    public static boolean isChinese(Context context) {
        Locale currentLaungage = getCurrentLaungage(context);
        return "zh".equals(currentLaungage.getLanguage());
    }

    public static String loacalToString(Locale local) {
        if ("zh".equals(local.getLanguage())) {
            return "中文";
        } else if ("en".equals(local.getLanguage())){
            return "English";
        } else {
            return "unKnow";
        }
    }

}