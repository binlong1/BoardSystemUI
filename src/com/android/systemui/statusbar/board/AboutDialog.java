package com.android.systemui.statusbar.board;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;
import android.widget.ImageView;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.content.Intent;

import java.io.File;

import com.android.systemui.R;
import com.android.systemui.statusbar.phone.PhoneStatusBar;

/**
 * Class  Name: AboutPopup
 * Description:
 * Created by bruce on 17/8/12
 */
class AboutDialog extends BaseDialog {
    private static final String TAG = "AboutDialog";
    Context boardContext;
    SharedPreferences boardSp;
    private String boardType = "";

    private SettingDialog settingDialog;

    AboutDialog(Context context) {
        super(context);
        initBoardSp();
    }

    public void setSettingDialog(SettingDialog settingDialog) {
        this.settingDialog = settingDialog;
    }

    @Override
    protected int getLayoutResId() {
        if (boardSp != null)
            boardType = boardSp.getString("boardType", "");
        if (TextUtils.equals(boardType, "CUSTOMER")) {
            return R.layout.popup_about;
        } else if (TextUtils.equals(boardType, "COMPANY")) {
            return R.layout.popup_about_network;
        } else {
            return R.layout.popup_about_individual;
        }
    }

    @Override
    protected void initView(View view) {
        TextView tvBoardName = (TextView) view.findViewById(R.id.tv_board_name);
        TextView tvRegisterInfo = (TextView) view.findViewById(R.id.tv_register_info);
        TextView tvHohoMsg = (TextView) view.findViewById(R.id.tv_hoho_msg);
        ImageView hohoLogo = (ImageView) view.findViewById(R.id.iv_hoho_logo);

        if (boardSp == null) return;
        tvBoardName.setText(mContext.getString(R.string.board_name,
                boardSp.getString("board_name", "")));
        tvRegisterInfo.setText(mContext.getString(R.string.register_info,
                boardSp.getString("board_address", ""), boardSp.getString("regist_time", "")));
        tvHohoMsg.setText(boardSp.getString("hoho_msg", ""));
        hohoLogo.setImageResource(R.drawable.hoho_logo);

        if (!TextUtils.equals(boardType, "INDIVIDUAL")) {
            TextView tvFullName = (TextView) view.findViewById(R.id.tv_full_name);
            ImageView customLogo = (ImageView) view.findViewById(R.id.iv_custom_logo);
            tvFullName.setText(boardSp.getString("fullName", ""));
            String cuLogo = Environment.getExternalStorageDirectory() + "/board/images/"
                    + getFileNameFromPath(boardSp.getString("customer_logo", ""));
            customLogo.setImageURI(Uri.fromFile(new File(cuLogo)));
        }

        if (TextUtils.equals(boardType, "CUSTOMER")) {
            TextView tvSupplierMsg = (TextView) view.findViewById(R.id.tv_supplier_msg);
            ImageView supplierLogo = (ImageView) view.findViewById(R.id.iv_supplier_logo);
            tvSupplierMsg.setText(boardSp.getString("supplier_msg", ""));
            String suLogo = Environment.getExternalStorageDirectory() + "/board/images/"
                    + getFileNameFromPath(boardSp.getString("supplier_logo", ""));
            supplierLogo.setImageURI(Uri.fromFile(new File(suLogo)));
        }

        if (PhoneStatusBar.HAS_UPDATE_PACKAGE) {
            view.findViewById(R.id.btn_version_introduce).setVisibility(View.GONE);
            view.findViewById(R.id.btn_version_update).setVisibility(View.VISIBLE);
        } else {
            view.findViewById(R.id.btn_version_introduce).setVisibility(View.GONE);
            view.findViewById(R.id.btn_version_update).setVisibility(View.GONE);
        }
        setClickListener(view.findViewById(R.id.btn_version_introduce));
        setClickListener(view.findViewById(R.id.btn_version_update));
        setClickListener(view.findViewById(R.id.btn_reregister));

        setClickListener(view.findViewById(R.id.btn_settings));
    }

    MoreSettingsDialog moreSettingsDialog;

    @Override
    public void onViewClick(View v) {
        if (v.getId() == R.id.btn_reregister) {
            if (settingDialog.getRegisterDialog() != null) {
                settingDialog.getRegisterDialog().show();
            } else {
                new RegisterDialog(mContext).show();
            }
        } else if (v.getId() == R.id.btn_version_introduce) {
        } else if (v.getId() == R.id.btn_version_update) {
            Intent updateIntent = new Intent("android.rockchip.update.service.INSTALLNOW");
            if (updateIntent != null) mContext.sendBroadcast(updateIntent);
        } else if (v.getId() == R.id.btn_settings) {
            if (moreSettingsDialog == null) {
                moreSettingsDialog = new MoreSettingsDialog(mContext);
            }
            moreSettingsDialog.show();
        }
    }

    /**
     * get a file name from the whole path
     */

    private String getFileNameFromPath(String path) {
        if (path == null) return "";
        String fileName = path;
        int start = path.lastIndexOf("/");
        int end = path.length();
        if (start != -1 && end != -1) {
            fileName = path.substring(start + 1, end);
        }
        return fileName;
    }

    private void initBoardSp() {
        try {
            boardContext = mContext.createPackageContext(
                    "com.w2here.board", Context.CONTEXT_IGNORE_SECURITY);
            boardSp = boardContext.getSharedPreferences("hhapp_base_cache",
                    Context.MODE_MULTI_PROCESS);
            boardType = boardSp.getString("boardType", "");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

}
