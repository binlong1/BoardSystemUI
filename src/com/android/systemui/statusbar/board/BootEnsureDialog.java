package com.android.systemui.statusbar.board;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.systemui.R;

public class BootEnsureDialog extends BaseDialog {

    public static final int TYPE_POWER_OFF = 1;
    public static final int TYPE_POWER_REBOOT = 2;

    TextView tvMsg;
    TextView tvRight;
    TextView tvLeft;
    ImageView ivIcon;

    private int mOperateType;

    public BootEnsureDialog(Context context, int type) {
        super(context);
        mOperateType = type;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.popup_booter_ensure;
    }

    @Override
    protected void initView(View view) {
        tvMsg = (TextView) view.findViewById(R.id.tv_msg);
        tvRight = (TextView) view.findViewById(R.id.tv_right);
        tvLeft = (TextView) view.findViewById(R.id.tv_left);
        ivIcon = (ImageView) view.findViewById(R.id.iv);
        if (mOperateType == TYPE_POWER_OFF) {
            ivIcon.setImageResource(R.drawable.booter);
            tvMsg.setText(mContext.getString(R.string.ensure_power_off));
            tvRight.setText(mContext.getString(R.string.power_off));
        } else if (mOperateType == TYPE_POWER_REBOOT) {
            ivIcon.setImageResource(R.drawable.reboot);
            tvMsg.setText(mContext.getString(R.string.ensure_power_reboot));
            tvRight.setText(mContext.getString(R.string.power_reboot));
        }

        tvLeft.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dismiss();
            }
        });
        tvRight.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (mOperateType == TYPE_POWER_OFF) {
                    Intent i = new Intent("android.intent.action.ACTION_REQUEST_SHUTDOWN");
                    i.putExtra("android.intent.extra.KEY_CONFIRM", false);
                    i.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(i);
                } else if (mOperateType == TYPE_POWER_REBOOT) {
                    PowerManager powerManager =
                            (PowerManager) mContext.getSystemService(Service.POWER_SERVICE);
                    powerManager.reboot("");
                }
                dismiss();
            }
        });
    }

}
