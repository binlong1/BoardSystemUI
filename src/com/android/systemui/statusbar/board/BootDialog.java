package com.android.systemui.statusbar.board;

import android.content.Context;
import android.view.View;
import android.app.Dialog;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;

import com.android.systemui.R;

public class BootDialog extends BaseDialog {
    public Context mContext;
    protected View baseView;
    protected Dialog mDialog;
    private WindowManager.LayoutParams wmDParams = null;
    DisplayMetrics mDisplayMetrics = new DisplayMetrics();
    Display mDisplay;
    WindowManager wm;
    LayoutInflater layoutInflater;

    public BootDialog(Context context) {
        super(context);
        this.mContext = context;
    }

    protected int getLayoutResId() {
        return R.layout.popup_boot_choose;
    }

    @Override
    protected void initView(View view) {
        setClickListener(view.findViewById(R.id.ll_power_off));
        setClickListener(view.findViewById(R.id.ll_power_reboot));
    }

    @Override
    protected void onViewClick(View view) {
        if (view.getId() == R.id.ll_power_off) {
            new BootEnsureDialog(mContext, BootEnsureDialog.TYPE_POWER_OFF).show();
        } else if (view.getId() == R.id.ll_power_reboot) {
            new BootEnsureDialog(mContext, BootEnsureDialog.TYPE_POWER_REBOOT).show();
        }
    }

}
