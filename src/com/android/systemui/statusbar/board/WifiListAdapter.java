package com.android.systemui.statusbar.board;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.support.annotation.LayoutRes;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.BaseAdapter;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import com.android.systemui.R;


public class WifiListAdapter extends BaseAdapter {

    private Context context;
    private List<ScanResult> scanResults = new ArrayList<>();
    private String connectedBSSID;
    private String connectedDescription;
    private LayoutInflater inflater;
    private String status = "";

    public WifiListAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }

    public void setData(List<ScanResult> data) {
        this.scanResults = data;
        if (this.scanResults == null) this.scanResults = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return scanResults.size();  //条目数量
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_wifi_content, null);
        }
        final ScanResult item = scanResults.get(position);

        TextView tvWifiName = (TextView) convertView.findViewById(R.id.tv_wifi_name);
        final TextView tvDescription = (TextView) convertView.findViewById(R.id.tv_wifi_description);
        TextView eloam_tip = (TextView) convertView.findViewById(R.id.eloam_tip);
        ImageView ivWifSignal = (ImageView) convertView.findViewById(R.id.iv_wifi_signal);
        ImageView ivDescription = (ImageView) convertView.findViewById(R.id.iv_wifi_description);

        tvWifiName.setText(item.SSID);
        if (item.SSID.contains("Eloam")) {
            eloam_tip.setVisibility(View.VISIBLE);
            ivDescription.setImageResource(R.drawable.eloam_icon);
            ivDescription.setVisibility(View.VISIBLE);
        } else {
            // 加密 or 未加密
            eloam_tip.setVisibility(View.GONE);
            ivDescription.setImageResource(R.drawable.wifi_private);
            ivDescription.setVisibility(TextUtils.isEmpty(getEncryption(item.capabilities))
                    ? View.INVISIBLE : View.VISIBLE);
        }

        setWifiLevel(ivWifSignal, item.level);
        //已连接 or 加密方式
//        tvDescription.setText(TextUtils.equals(connectedBSSID, item.BSSID)
//                ? connectedDescription : getEncryption(item.capabilities));

        if (TextUtils.equals(connectedBSSID, item.BSSID)) {
            tvDescription.setText(connectedDescription);
        } else {
            tvDescription.setText(getEncryption(item.capabilities));
        }

        return convertView;
    }

    private void setWifiLevel(ImageView imageView, int level) {
        if (level <= 0 && level >= -50) {
            imageView.setImageResource(R.drawable.wifi_signal_3);
        } else if (level < -50 && level >= -80) {
            imageView.setImageResource(R.drawable.wifi_signal_2);
        } else if (level < -80 && level >= -100) {
            imageView.setImageResource(R.drawable.wifi_signal_1);
        } else {
            imageView.setImageResource(R.drawable.wifi_signal_0);
        }
    }

    public void setConnectedBSSID(String connectedBSSID) {
        this.connectedBSSID = connectedBSSID;
        this.connectedDescription = context.getString(R.string.wifi_connected);
        if (!TextUtils.isEmpty(connectedBSSID)) {
            scanResults = scanResults;
            if (scanResults.size() > 1) {
                for (int i = 1; i < scanResults.size(); i++) {
                    if (scanResults.get(i).BSSID.equals(connectedBSSID)) {
                        ScanResult temp = scanResults.get(i);
                        scanResults.remove(i);
                        scanResults.add(0, temp);
                        BoardUtil.isWifiOnline(context, new BoardUtil.NetworkOnlineListener() {
                            @Override
                            public void onNetworkOnline(final boolean online) {
                                if (!online) {
                                    connectedDescription = context.getString(R.string.wifi_no_net);
                                } else {
                                    connectedDescription = context.getString(R.string.wifi_connected);
                                }
                            }

                        });
                        this.notifyDataSetChanged();
                        return;
                    }
                }
            }
        }
        this.notifyDataSetChanged();
    }

    public void setConnectingDescription(String connectedBSSID, String connectedDescription) {
        this.connectedBSSID = connectedBSSID;
        this.connectedDescription = connectedDescription;
    }

    public String getEncryption(String capabilities) {
        if (capabilities == null) return "";
        if (capabilities.contains("WPA2-PSK-CCMP") && capabilities.contains("WPA-PSK-CCMP"))
            return "WPA2/WPA";
        if (capabilities.contains("WPA2"))
            return "WPA2";
        if (capabilities.contains("WPA"))
            return "WPA";
        if (capabilities.contains("WEP"))
            return "WEP";
        if (capabilities.contains("WAPI"))
            return "WAPI";
        return "";
    }

}
