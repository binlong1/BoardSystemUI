package com.android.systemui.statusbar.board;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.util.Log;
import android.text.TextUtils;
import android.os.Handler;

import com.android.systemui.R;

public class RegisterDialog extends BaseDialog {

    private ImageView ivQrCode;
    private LinearLayout llNetTip;
    private TextView tvNetSetting;
    private TextView tvName;
    private TextView tvId;
    private TextView tvCompany;
    private Intent intent;
    private InternetDialog internetDialog;
    private TextView loading;
    private String loadingPoint;
    private int loadingCount = 0;
    private MoreSettingsDialog moreSettingsDialog;

    private Context boardContext;
    private SharedPreferences boardSp;

    private BroadcastReceiver mBoardReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (TextUtils.equals("android.intent.board.REGISTER_TOKEN", action)) {
                if (TextUtils.equals("success", intent.getStringExtra("result"))) {
                    String token = intent.getStringExtra("token");
                    Log.e("ConnectPhoneDialog", "register token :" + token);
                    if (TextUtils.isEmpty(token)) {
                        ivQrCode.setBackgroundResource(R.drawable.qrcode_no_net);
                        return;
                    }
                    Bitmap bitmap = QRCodeEncoderUtil.syncEncodeQRCode(token, BoardUtil.dip2px(100));
                    llNetTip.setVisibility(View.GONE);
                    ivQrCode.setImageBitmap(bitmap);
                } else {
                    llNetTip.setVisibility(View.VISIBLE);
                    ivQrCode.setBackgroundResource(R.drawable.qrcode_no_net);
                }
            }
        }
    };

    public RegisterDialog(Context context) {
        super(context);
        initBoardSp();
        registerBoardReceiver();
    }

    private void registerBoardReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.board.REGISTER_TOKEN");
        mContext.registerReceiver(mBoardReceiver, filter, null, null);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.pop_setting_register;
    }

    @Override
    protected void initView(View view) {
        ivQrCode = (ImageView) view.findViewById(R.id.iv_qrcode);
        llNetTip = (LinearLayout) view.findViewById(R.id.ll_net_tip);
        tvNetSetting = (TextView) view.findViewById(R.id.tv_net_setting);
        tvName = (TextView) view.findViewById(R.id.tv_name);
        tvId = (TextView) view.findViewById(R.id.tv_id);
        tvCompany = (TextView) view.findViewById(R.id.tv_company);
        TextView tvTip = (TextView) view.findViewById(R.id.tv_tip);
        loading = (TextView) view.findViewById(R.id.loading);
        loadingCount = 0;
        handler.postDelayed(loadingRunnable, 0);

        tvTip.getPaint().setFakeBoldText(true);

        tvName.setText(mContext.getResources().getString(R.string.product_version,
                boardSp.getString("board_verson", ""),boardSp.getString("rom_verson", "")));
        tvId.setText(mContext.getResources().getString(R.string.product_code,boardSp.getString("board_serial", "")));
        tvCompany.setText(mContext.getResources().getString(R.string.product_copyright));

        setClickListener(tvNetSetting);
        setClickListener(view.findViewById(R.id.btn_settings));
    }

    @Override
    public void show() {
        super.show();
        if (!BoardUtil.isNetAvailable(mContext)) {
            ivQrCode.setBackgroundResource(R.drawable.qrcode_no_net);
            llNetTip.setVisibility(View.VISIBLE);
            return;
        }
        getInfoQrCodeUrl();
    }

    private Handler handler = new Handler();
    private Runnable loadingRunnable = new Runnable() {
        @Override
        public void run() {
            if (loadingCount > 3) {
                loadingCount = 0;
            }
            if (loadingCount == 0) {
                loadingPoint = "   ";
            } else if (loadingCount == 1) {
                loadingPoint = ".  ";
            } else if (loadingCount == 2) {
                loadingPoint = ".. ";
            } else if (loadingCount == 3) {
                loadingPoint = "...";
            }
            loadingCount = loadingCount + 1;
            loading.setText(mContext.getResources().getString(R.string.getting_qr_code) + loadingPoint);
            handler.postDelayed(loadingRunnable, 500);
        }
    };

    private void getInfoQrCodeUrl() {
        if (intent == null) intent = new Intent("com.w2here.board.QRCODE_REGISTER");
        mContext.sendBroadcast(intent);
    }

    @Override
    public void onViewClick(View v) {
        if (v == tvNetSetting) {
            getNetSettingDialog().show();
        } else if (v.getId() == R.id.btn_settings) {
            if (moreSettingsDialog == null) {
                moreSettingsDialog = new MoreSettingsDialog(mContext);
            }
            moreSettingsDialog.show();
        }
    }

    @Override
    public void dismiss() {
        if (null != mDialog) mDialog.dismiss();
        handler.removeCallbacks(loadingRunnable);
    }

    private InternetDialog getNetSettingDialog() {
        if (internetDialog == null)
            internetDialog = new InternetDialog(mContext);
        return internetDialog;
    }

    private void initBoardSp() {
        try {
            boardContext = mContext.createPackageContext(
                    "com.w2here.board", Context.CONTEXT_IGNORE_SECURITY);
            boardSp = boardContext.getSharedPreferences("hhapp_base_cache",
                    Context.MODE_MULTI_PROCESS);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

}
