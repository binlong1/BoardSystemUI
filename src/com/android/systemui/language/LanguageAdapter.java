package com.android.systemui.language;

import android.app.Service;
import android.content.Context;
import android.os.PowerManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import com.android.systemui.R;
import com.android.systemui.statusbar.board.LanguageEnsureDialog;

/**
 * Created by drolmen on 2018/2/6.
 */

public class LanguageAdapter extends BaseAdapter implements View.OnClickListener {

    private ArrayList<Locale> mSupportLanguages ;
    private int selectPosition;
    private Context mContext ;

    public LanguageAdapter(Context context) {
        mContext = context ;
        initSupportLanguage();
        initSelectPosition(context);
    }

    private void initSupportLanguage() {
        mSupportLanguages = new ArrayList<>();
        mSupportLanguages.add(Locale.CHINA);
        mSupportLanguages.add(Locale.US);
    }

    private void initSelectPosition(Context context) {
        String language = LanguageUtils.getCurrentLaungage(context).getLanguage();
        for (int i = 0; i < mSupportLanguages.size(); i++) {
            if (language.equals(mSupportLanguages.get(i).getLanguage())) {
                selectPosition = i;
                return;
            }
        }
    }


    @Override
    public void onClick(View v) {
        int position = (int) v.getTag();
        if (position == selectPosition) {
            return;
        }
        Locale selectlocal = getItem(position);
        new LanguageEnsureDialog(mContext,selectlocal).show(); ;
    }

    @Override
    public int getCount() {
        return mSupportLanguages.size();
    }

    @Override
    public Locale getItem(int position) {
        return mSupportLanguages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.item_language, parent, false);
            convertView.setOnClickListener(this);
            vh = new ViewHolder(convertView);
            convertView.setTag(R.id.tag, vh);
        } else {
            vh = (ViewHolder) convertView.getTag(R.id.tag);
        }

        Locale locale = mSupportLanguages.get(position);

        vh.tvName.setText(LanguageUtils.loacalToString(locale));
        vh.ivSeletedStatus.setVisibility(position == selectPosition ?
                View.VISIBLE : View.GONE);

        vh.itemView.setTag(position);

        return convertView;
    }

    protected static class ViewHolder {

        public TextView tvName ;
        public ImageView ivSeletedStatus ;
        public View itemView;
        public ViewHolder(View itemView) {
            this.itemView = itemView;
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            ivSeletedStatus = (ImageView) itemView.findViewById(R.id.iv_selete_status);
        }
    }
}
