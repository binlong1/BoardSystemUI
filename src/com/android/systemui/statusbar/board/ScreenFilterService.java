package com.android.systemui.statusbar.board;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.android.systemui.R;
import com.android.systemui.statusbar.policy.PreviewInflater;

public class ScreenFilterService extends Service {
    @SuppressLint("StaticFieldLeak")
    static LinearLayout linearLayout;
    static WindowManager.LayoutParams layoutParams;
    static WindowManager windowManager;
    static boolean isInit = false;
    private static int filterColor = 0x33ff8c00;
    static LinearLayout llEyeProtection;
    static WindowManager.LayoutParams lp;
    static boolean isEyeProtect = false;

    @Override
    public void onCreate() {
        super.onCreate();
        createScreenFilter();
        setEyeProtection();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isInit = false;
        try {
            windowManager.removeViewImmediate(linearLayout);
        } catch (Exception ignored) {
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @SuppressWarnings(value = "all")
    private void createScreenFilter() {
        WindowManager windowManager_tmp = (WindowManager)
                getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        layoutParams = new WindowManager.LayoutParams();
        windowManager = (WindowManager) getApplication().getSystemService(Context.WINDOW_SERVICE);
        layoutParams.type = 2027;
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = windowManager_tmp.getDefaultDisplay().getHeight() +
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 80,
                        getApplicationContext().getResources().getDisplayMetrics());
        layoutParams.format = PixelFormat.TRANSLUCENT;

        LayoutInflater layoutInflater = LayoutInflater.from(getApplication());
        linearLayout = (LinearLayout) layoutInflater.inflate(R.layout.screen_filter, null);
        linearLayout.setBackgroundColor(Color.BLACK);
    }

    public static void updateScreenFilter(float brightness) {
        updateBrightness(brightness);
        if (isInit) {
            windowManager.updateViewLayout(linearLayout, layoutParams);
        } else {
            isInit = true;
            try {
                windowManager.addView(linearLayout, layoutParams);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void updateBrightness(float brightness) {
        layoutParams.alpha = (80 - brightness) / 100;//hdmi亮度调节无效，用alpha代替

        layoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                | WindowManager.LayoutParams.FLAG_FULLSCREEN
                | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;
    }

    public static void removeScreenFilter() {
        windowManager.removeViewImmediate(linearLayout);
        isInit = false;
    }

    @SuppressWarnings(value = "all")
    private void setEyeProtection() {
        WindowManager windowManager_tmp = (WindowManager)
                getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        lp = new WindowManager.LayoutParams();
        lp.type = 2027;
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = windowManager_tmp.getDefaultDisplay().getHeight()
                + (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 80,
                getApplicationContext().getResources().getDisplayMetrics());
        lp.format = PixelFormat.TRANSLUCENT;

        LayoutInflater layoutInflater = LayoutInflater.from(getApplication());
        llEyeProtection = (LinearLayout) layoutInflater.inflate(R.layout.screen_filter, null);
        llEyeProtection.setBackgroundColor(filterColor);
    }

    public static void setEyeProtectionMode(boolean mode) {
        lp.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                | WindowManager.LayoutParams.FLAG_FULLSCREEN
                | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;
        if (mode) {
            if (!isEyeProtect) {
                try {
                    windowManager.addView(llEyeProtection, lp);
                    isEyeProtect = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            windowManager.removeViewImmediate(llEyeProtection);
            isEyeProtect = false;
        }
    }

}
