package com.android.systemui.statusbar.board;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.android.systemui.R;

import java.util.List;

/**
 * Created by Mansoul on 17/11/30.
 */

public class BgPaintStyleAdapter extends BaseAdapter {
    private List<BgPaintStyle> bgPaintStyles;
    private int selectedPos;

    public BgPaintStyleAdapter(List<BgPaintStyle> bgPaintStyles) {
        this.bgPaintStyles = bgPaintStyles;
    }

    @Override
    public int getCount() {
        return bgPaintStyles.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_bg_paint_style, null);
        }
        BgPaintStyle style = bgPaintStyles.get(position);

        ImageView ivSelected = (ImageView) convertView.findViewById(R.id.iv_selected);
        ImageView ivStyle = (ImageView) convertView.findViewById(R.id.iv_style);

        ivSelected.setVisibility(style.isSelected ? View.VISIBLE : View.INVISIBLE);
        ivStyle.setBackgroundResource(style.resId);

        return convertView;
    }

    public void setSelectedPos(int pos) {
        selectedPos = pos;
    }

    public int getSelectedPos() {
        return selectedPos;
    }

    public static class BgPaintStyle {
        public int resId;
        public boolean isSelected;
    }
}
