package com.android.systemui.statusbar.board;

import android.app.Dialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.PowerManager;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.systemui.R;
import com.android.systemui.language.LanguageUtils;

import java.util.Locale;

public class LanguageEnsureDialog extends BaseDialog {


    TextView tvRight;
    TextView tvLeft;

    private Locale mDestLocal ;

    public LanguageEnsureDialog(Context context, Locale locale) {
        super(context);
        mDestLocal = locale;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.popup_language_switch_ensure;
    }

    @Override
    protected void initView(View view) {
        tvRight = (TextView) view.findViewById(R.id.tv_right);
        tvLeft = (TextView) view.findViewById(R.id.tv_left);
        tvLeft.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dismiss();
            }
        });
        tvRight.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                LanguageUtils.switchLanguage(mDestLocal);
                PowerManager powerManager =
                        (PowerManager) mContext.getSystemService(Service.POWER_SERVICE);
                powerManager.reboot("");
            }
        });
    }

    @Override
    public void show() {
        baseView = getContentView();
        mDialog = new Dialog(mContext, R.style.DefaultDialog);
        wmDParams = mDialog.getWindow().getAttributes();
        wmDParams.x = 100;
        wmDParams.y = 372;
        mDialog.setContentView(baseView);
        mDialog.getWindow().setAttributes(wmDParams);
        mDialog.getWindow().setGravity(Gravity.RIGHT | Gravity.BOTTOM);
        mDialog.getWindow().setType(2002);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                dowhenDismiss();
            }
        });
        mDialog.show();
        sCurrentShowPopup = this;
    }

    @Override
    public boolean dismissOthers() {
        return false;
    }
}
