package com.android.systemui.statusbar.board;

import android.app.Dialog;
import android.content.Context;
import android.content.ComponentName;
import android.content.pm.PackageManager;
import android.content.SharedPreferences;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListView;
import android.text.TextUtils;
import android.widget.TextView;

import com.android.systemui.R;

import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.io.IOException;

public class ClearOptionsDialog extends BaseDialog {
    public final static int CLEAR_TYPE_DOCUMENT = 0;
    public final static int CLEAR_TYPE_PLAY = 1;
    public final static int CLEAR_TYPE_PROJECTION = 2;

    private TextView tvClearDirect;
    private TextView tvClearKeepBackground;
    private TextView tvClearCancel;
    private ClickCallBack clickCallBack;

    public ClearOptionsDialog(Context context) {
        super(context);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.clear_options_dialog;
    }

    @Override
    protected void initView(View view) {
        tvClearDirect = (TextView) view.findViewById(R.id.tv_clear_direct);
        tvClearKeepBackground = (TextView) view.findViewById(R.id.tv_clear_keep_background);
        tvClearCancel = (TextView) view.findViewById(R.id.tv_clear_cancel);
        setClickListener(tvClearDirect);
        setClickListener(tvClearKeepBackground);
        setClickListener(tvClearCancel);
    }

    public void setText(int type) {
        switch (type) {
            case CLEAR_TYPE_DOCUMENT:
                tvClearDirect.setText(mContext.getString(R.string.clear_direct, mContext.getString(R.string.file_close)));
                tvClearKeepBackground.setText(mContext.getString(R.string.clear_keep_background, mContext.getString(R.string.file)));
                break;
            case CLEAR_TYPE_PLAY:
                tvClearDirect.setText(mContext.getString(R.string.clear_direct, mContext.getString(R.string.play_close)));
                tvClearKeepBackground.setText(mContext.getString(R.string.clear_keep_background, mContext.getString(R.string.play)));
                break;
            case CLEAR_TYPE_PROJECTION:
                tvClearDirect.setText(mContext.getString(R.string.clear_direct, mContext.getString(R.string.cast_close)));
                tvClearKeepBackground.setText(mContext.getString(R.string.clear_keep_background, mContext.getString(R.string.cast)));
                break;
        }
    }

    @Override
    public void setClickListener(View v) {
        switch (v.getId()) {
            case R.id.tv_clear_direct:

                break;
            case R.id.tv_clear_keep_background:

                break;
            case R.id.tv_clear_cancel:
                break;
        }
    }

    public void setClickCallBack(ClickCallBack clickCallBack) {
        this.clickCallBack = clickCallBack;
    }

    public interface ClickCallBack {
        void clearDirect();

        void clearKeepbackground();
    }

}