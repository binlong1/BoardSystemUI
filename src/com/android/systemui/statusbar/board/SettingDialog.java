package com.android.systemui.statusbar.board;

import android.app.Dialog;
import android.content.Context;
import android.content.ComponentName;
import android.content.pm.PackageManager;
import android.content.SharedPreferences;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListView;
import android.text.TextUtils;

import com.android.systemui.R;

import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.io.IOException;

public class SettingDialog extends BaseDialog implements SettingAdapter.ItemClickCallback {

    private static final String TAG = "SettingDialog";

    public static Boolean isShown = false;
    private Intent intent;
    private SettingAdapter settingAdapter;
    List<String> settings = new ArrayList<>();

    private ConnectPhoneDialog connectPhoneDialog;
    private RegisterDialog registerDialog;
    private AboutDialog aboutDialog;
    private InternetDialog internetDialog;
    private BootDialog bootDialog;


    public SettingDialog(Context context) {
        super(context);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.setting_dialog;
    }

    @Override
    protected void initView(View view) {
        ListView listView = (ListView) view.findViewById(R.id.lv_setting);
        settingAdapter = new SettingAdapter(mContext, this);
        initSettingData();
        settingAdapter.setData(settings);
        listView.setAdapter(settingAdapter);
    }

    @Override
    public void show() {
        if (mDialog == null) {
            initDialog();
        } else {
            initSettingData();
            settingAdapter.notifyDataSetChanged();
        }
        mDialog.show();
//        sCurrentShowPopup = this;
    }

    private void initDialog() {
        View settingView = getContentView();
        mDialog = new Dialog(mContext, R.style.FullHeightDialog);
        WindowManager.LayoutParams wmDParams = mDialog.getWindow().getAttributes();
        wmDParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        wmDParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        wmDParams.x = BoardUtil.dip2px(10);
        wmDParams.format = 1;
        mDialog.setContentView(settingView);
        mDialog.getWindow().setAttributes(wmDParams);
        mDialog.getWindow().setGravity(Gravity.RIGHT | Gravity.BOTTOM);
        mDialog.getWindow().setType(2002);
        mDialog.setCanceledOnTouchOutside(true);
    }

    public ConnectPhoneDialog getConnectPhoneDialog() {
        if (connectPhoneDialog == null)
            connectPhoneDialog = new ConnectPhoneDialog(mContext);
        return connectPhoneDialog;
    }

    public RegisterDialog getRegisterDialog() {
        if (registerDialog == null)
            registerDialog = new RegisterDialog(mContext);
        return registerDialog;
    }

    public AboutDialog getAboutDialog() {
        if (aboutDialog == null) {
            aboutDialog = new AboutDialog(mContext);
            aboutDialog.setSettingDialog(this);
        }
        return aboutDialog;
    }

    public InternetDialog getInternetDialog() {
        if (internetDialog == null)
            internetDialog = new InternetDialog(mContext);
        return internetDialog;
    }

    public BootDialog getBootDialog() {
        if (bootDialog == null)
            bootDialog = new BootDialog(mContext);
        return bootDialog;
    }

    @Override
    public void itemClick(String item) {
        dismiss();
        switch (item) {
            case SettingAdapter.REGISTER:
                getRegisterDialog().show();
                break;
            case SettingAdapter.CONNECT_PHONE:
                getConnectPhoneDialog().show();
                break;
            case SettingAdapter.MIRROR:
                startBoardActivity("mirror");
                break;
            case SettingAdapter.ABOUT:
                getAboutDialog().show();
                break;
            case SettingAdapter.LOCAL_STORAGE:
                startBoardActivity("file");
                break;
            case SettingAdapter.INTERNET:
                getInternetDialog().show();
                break;
            case SettingAdapter.SHUTDOWN:
                setAirpin();
                getBootDialog().show();
                break;
            case SettingAdapter.BROWSER:
                startBoardActivity("browser");
                break;
        }
    }

    private void startBoardActivity(String type) {
        if (intent == null) {
            intent = new Intent("board.intent.action.MAIN");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        intent.putExtra("type", type);
        mContext.startActivity(intent);
    }

    private void initSettingData() {
        settings.clear();
        if (isRegister()) { //已注册
            settings.add(SettingAdapter.CONNECT_PHONE);
            settings.add(SettingAdapter.MIRROR);
            if (isShowBrowser()) {
                settings.add(SettingAdapter.BROWSER);
            }
            settings.add(SettingAdapter.LOCAL_STORAGE);
            settings.add(SettingAdapter.INTERNET);
            settings.add(SettingAdapter.ABOUT);
        } else { //未注册
            settings.add(SettingAdapter.REGISTER);
            settings.add(SettingAdapter.CONNECT_PHONE);
            settings.add(SettingAdapter.MIRROR);
            if (isShowBrowser()) {
                settings.add(SettingAdapter.BROWSER);
            }
            settings.add(SettingAdapter.LOCAL_STORAGE);
            settings.add(SettingAdapter.INTERNET);
        }
        settings.add(SettingAdapter.SHUTDOWN);
    }

    public boolean isRegister() {
        boolean isRegister = false;
        try {
            Context boardContext = mContext.createPackageContext(
                    "com.w2here.board", Context.CONTEXT_IGNORE_SECURITY);
            SharedPreferences boardSp = boardContext.getSharedPreferences("hhapp_base_cache",
                    Context.MODE_MULTI_PROCESS);
            isRegister = !TextUtils.isEmpty(boardSp.getString("regist_time", ""));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return isRegister;
    }

    public boolean isShowBrowser() {
        boolean visible = true;
        try {
            Context boardContext = mContext.createPackageContext(
                    "com.w2here.board", Context.CONTEXT_IGNORE_SECURITY);
            SharedPreferences boardSp = boardContext.getSharedPreferences("hhapp_base_cache",
                    Context.MODE_MULTI_PROCESS);
            visible = boardSp.getBoolean("browser_visible", true);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return visible;
    }

    public void setAirpin() {
        try {
            File file = new File("/data/data/com.waxrain.airplaydmr/shared_prefs/atest.xml");
            file.createNewFile();

            Context boardContext = mContext.createPackageContext(
                    "com.waxrain.airplaydmr", Context.CONTEXT_IGNORE_SECURITY);
            SharedPreferences sp = boardContext.getSharedPreferences("com.waxrain.airplaydmr",
                    Context.MODE_MULTI_PROCESS | Context.MODE_WORLD_READABLE | Context.MODE_WORLD_WRITEABLE);
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("VERSION", "4.0.10");
            editor.commit();

        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

}
