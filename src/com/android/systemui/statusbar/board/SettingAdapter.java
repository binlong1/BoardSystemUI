package com.android.systemui.statusbar.board;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.systemui.R;

import java.util.*;

import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import android.view.View.OnClickListener;

/**
 * Class  Name: SettingAdapter
 * Description:
 * Created by bruce on 17/9/11
 */
public class SettingAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private Context context;
    private static List<String> data = new ArrayList<>();//为条目提供数据

    public static final String REGISTER = "register";//注册
    public static final String CONNECT_PHONE = "connect_phone";//连接手机
    public static final String MIRROR = "mirror";//直接投屏
    public static final String LOCAL_STORAGE = "local_storage";//本机存储
    public static final String INTERNET = "internet";//网络设置
    public static final String SHUTDOWN = "shutdown";//关机/重启
    public static final String ABOUT = "about";//关于
    public static final String BROWSER = "browser";//浏览器

    private ItemClickCallback callback;

    public SettingAdapter(Context context, ItemClickCallback callback) {
        this.context = context;
        this.callback = callback;
        inflater = LayoutInflater.from(this.context);
    }

    public void setData(List<String> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();  //条目数量
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_setting, null);
        }
        final String item = data.get(position);

        RelativeLayout rlItem = (RelativeLayout) convertView.findViewById(R.id.rl_item);
        ImageView icon = (ImageView) convertView.findViewById(R.id.iv_setting);//设置每个条目的图标
        TextView text = (TextView) convertView.findViewById(R.id.tv_setting); //设置条目的文字说明
        final ImageView noNet = (ImageView) convertView.findViewById(R.id.iv_net_tip);
//        View divider = convertView.findViewById(R.id.setting_divider);

//        divider.setVisibility(View.VISIBLE);
        if (0 == position) {
            rlItem.setBackgroundResource(R.drawable.shape_top_radius_4dp_white);
        } else if (data.size() - 1 == position) {
            rlItem.setBackgroundResource(R.drawable.shape_bottom_radius_4dp_white);
//            divider.setVisibility(View.GONE);
        } else {
            rlItem.setBackgroundResource(R.drawable.bottom_stroke_1dp);
        }

        rlItem.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                callback.itemClick(item);
            }
        });

        switch (item) {
            case REGISTER:
                text.setText(R.string.str_register);
                icon.setImageResource(R.drawable.setting_register);
                break;
            case CONNECT_PHONE:
                text.setText(R.string.str_connect_phone);
                icon.setImageResource(R.drawable.setting_phone);
                break;
            case MIRROR:
                text.setText(R.string.str_setting_mirror);
                icon.setImageResource(R.drawable.setting_mirror);
                break;
            case BROWSER:
                text.setText(getBrowserName());
                icon.setImageResource(R.drawable.setting_browser);
                break;
            case INTERNET:
                text.setText(R.string.str_setting_net);
                icon.setImageResource(R.drawable.setting_internet);
                break;
            case SHUTDOWN:
                text.setText(R.string.str_power_off_restart);
                icon.setImageResource(R.drawable.setting_power);
                break;
            case ABOUT:
                text.setText(R.string.str_about);
                icon.setImageResource(R.drawable.setting_about);
                break;
            case LOCAL_STORAGE:
                text.setText(R.string.str_setting_storage);
                icon.setImageResource(R.drawable.setting_local_stroage);
                break;
        }

        noNet.setVisibility(View.GONE);
        if (item == INTERNET) {
            BoardUtil.isNetworkOnline(context, new BoardUtil.NetworkOnlineListener() {
                @Override
                public void onNetworkOnline(final boolean online) {
                    noNet.post(new Runnable() {
                        @Override
                        public void run() {
                            noNet.setVisibility(online ? View.GONE : View.VISIBLE);
                        }
                    });
                    //延迟20秒，然后断开当前wifi
//                    if (!online) {
//                        new Timer().schedule(new TimerTask() {
//                            public void run() {
//                                BoardUtil.isNetworkOnline(context, new BoardUtil.NetworkOnlineListener() {
//                                    @Override
//                                    public void onNetworkOnline(final boolean online) {
//                                        //断开当前wifi
//                                        WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
//                                        WifiInfo info = wifi.getConnectionInfo();
//                                        if (!online && !info.getSSID().contains("Eloam")) {
//                                            int networkID = info.getNetworkId();
//                                            wifi.disableNetwork(networkID);
//                                            wifi.disconnect();
//                                            wifi.removeNetwork(networkID);
//                                            wifi.saveConfiguration();
//                                        }
//                                    }
//                                });
//                            }
//                        }, 20000);
//                    }
                }

            });
        }

        return convertView;
    }

    public String getBrowserName() {
        String defaultName = context.getResources().getString(R.string.str_setting_browser);
        String name = defaultName;
        try {
            Context boardContext = context.createPackageContext(
                    "com.w2here.board", Context.CONTEXT_IGNORE_SECURITY);
            SharedPreferences boardSp = boardContext.getSharedPreferences("hhapp_base_cache",
                    Context.MODE_MULTI_PROCESS);
            name = boardSp.getString("browser_name", defaultName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return name;
    }

    public interface ItemClickCallback {
        public void itemClick(String item);
    }

}